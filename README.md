<div align="center">

# Actividad 8 - JavaScript - DOM - ROLL OF DICE

[![Estado de construcción](https://img.shields.io/static/v1?label=Estado%20de%20Construcción&message=Finalizado&color=brightgreen)](https://gitlab.com/gusgonza/mp04-actividad-8)

</div>

Esta actividad es una simple aplicación web que permite a los usuarios lanzar un número de dados y ver los resultados.

## Funcionalidad

La aplicación tiene un campo de entrada donde los usuarios pueden especificar el número de dados que desean lanzar. Una vez que se introduce un número, los usuarios pueden hacer clic en el botón "ROLL" para lanzar los dados.

La aplicación verifica si el número introducido es válido. Si el número no es válido (por ejemplo, si es menor o igual a 0, mayor que 50, o no es un número en absoluto), se muestra un mensaje de error en rojo.

Los resultados de los lanzamientos de dados se muestran en un contenedor de resultados debajo del botón "ROLL".

## Contenido del Repositorio

El repositorio contiene la siguiente estructura de carpetas y archivos:

- **assets**: Carpeta que contiene los recursos necesarios para la página web.
  - **css/**: Carpeta que contiene los archivos CSS.
  - **img/**: Carpeta que contiene las imágenes utilizadas en la página.
  - **js/**: Carpeta que contiene los archivos JavaScript.
- **index.html**: Archivo HTML que contiene la estructura de la página web.
- **Actividad-8.pdf**: Documento PDF que describe los requisitos de la actividad.
- **README.md**: Archivo README del proyecto.

